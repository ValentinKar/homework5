<?php 
//$twig = file_get_contents("./Twig-2.3.2/composer.json"); 
//var_dump($twig); 
//$twig = new Twig_Environment($loader);
//echo $twig->render('Hello {{ name }}!', array('name' => 'Fabien'));
mb_internal_encoding('utf-8');  //позволяет использовать все ф-ции для кириллицы
error_reporting(E_ALL);        //вывести на экран все ошибки
$content = file_get_contents("1.json");    //загружаю json-данные из файла
$people = json_decode($content, true);     //json-данные записываю в массив
?> 
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Notebook</title>
<style>
body {
 font-family: sans-serif;
 font-size: 15px;
}
table {
border: 1px solid black; 
padding: 4px;
}
td {
border: 1px solid black; 
padding: 4px;
}
</style>
</head>
<body>
<h1>Записная книжка</h1>
<?php foreach ($people as $key => $person) : ?>
<h3>
<?php 
echo $person['first_name'].' '.$person['last_name']; 
echo ' ('.$person['Comments'].')'; 
?>
</h3>
<table >
   <tr>
   <td>
    Имя:
   </td>
   <td>
    <?php echo $person['first_name']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Фамилия:
   </td>
   <td>
    <?php echo $person['last_name']; ?>
   </td>
  </tr>
 
  <tr>
   <td>
    Мобильный телефон:
   </td>
   <td>
    <?php echo $person['mobil_phone']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Адрес:
   </td>
   <td>
    <?php 
    foreach ($person['Adress'] As $Key1 => $Data1) 
    	{
    	echo $Data1.'<br/>';
    	} 
    ?>
   </td>
  </tr>

  <tr>
   <td>
    Организация:
   </td>
   <td>
    <?php echo $person['Organization']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Работа:
   </td>
   <td>
    <?php echo $person['Job']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Пояснения:
   </td>
   <td>
    <?php echo $person['Comments']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Netology:
   </td>
   <td>
    <?php echo $person['Netology']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Контакты:
   </td>
   <td>
    <?php 
    foreach ($person['Contacts'] As $Key2 => $Data2) 
    	{
    	echo $Data2.'<br/>';
    	} 
    ?>
   </td>
  </tr>

  <tr>
   <td>
    Электронная почта:
   </td>
   <td>
    <?php echo $person['Email']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Эл. адрес странички "В_контакте":
   </td>
   <td>
    <?php echo $person['vk']; ?>
   </td>
  </tr>

  <tr>
   <td>
    Эл. адрес странички "фейсбук":
   </td>
   <td>
    <?php echo $person['fb']; ?>
   </td>
  </tr>
</table>
<br><br>
<?php endforeach; ?>
</body>
</html>